# 1.3.6

* Added column for quantity and fixed formatting of it and price

# 1.3.5

* Resized starting window to avoid scrollbars
* Fix to Biography editor

# 1.3.4

* Compatibility with 0.5.3

# 1.3.3

* Fixed bug in FVTT 0.4.4

# 1.3.2

* Compatibility with Foundry VTT 0.4.4

# 1.3.1

* Updated Price Modifier to allow input/fine-tuning of percentage value

# 1.3.0

* Assigned price to spell scrolls based on formula of 10 * 2.6^(spell level)
* Added Price Modifier button to allow raising and lower of all item prices in the sheet
* Added Biography tab so that descriptions can be entered in case the sheet is for an NPC shopkeeper instead of an inanimate object like a chest
* Fixed bug with permission cycling on Token version of the sheet
* Removed "gp" from prices

# 1.2.0

* Updated inventory sections to match dnd5e item types

# 1.1.3

* Don't become default NPC sheet

# 1.1.1

*  Display item price on sheet

# 1.1

*  Forked from https://gitlab.com/hooking/foundry-vtt---loot-sheet-npc
*  Compatible with Foundry VTT 0.4.3